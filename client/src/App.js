import SignUp from './components/SignUp';

import Navbar from './components/Navbar';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import SignIn from './components/SignIn';
import { useSelector } from 'react-redux';
import Logout from './components/Logout';
import SidebarShipper from './components/SidebarShipper';
import { useState } from 'react';
import MenuShipper from './components/MenuShipper';
import { ErrorBoundary } from 'react-error-boundary';
import ErrorFallback from './components/ErrorFallback';
import HomePage from './components/HomePage';
import History from './components/History';
import SimpleMap from './components/SimpleMap';
import GenPdf from './components/GenPdf';
const theme = createTheme({
  palette: {
    mode: 'dark',
  },
});
function App() {
  const user = useSelector((state) => state.user);
  const [open, setOpen] = useState(false);
  const toggleOpen = () => {
    setOpen(!open);
  };
  return (
    <ThemeProvider theme={theme}>
      <div className='App'>
        <BrowserRouter>
          <Navbar toggleOpen={toggleOpen} />
          <div className='wrapper'>
            <div className='left-wrapper'>
              {user.currentUser ? <SidebarShipper open={open} /> : null}
            </div>
            <div className='main-wrapper'>
              <Routes>
                <Route path='/'>
                  {!user.currentUser ? (
                    <>
                      <Route path='register' element={<SignUp />}></Route>
                      <Route
                        path='login'
                        element={
                          <ErrorBoundary FallbackComponent={ErrorFallback}>
                            <SignIn />
                          </ErrorBoundary>
                        }
                      ></Route>
                    </>
                  ) : null}
                  <Route path='home' element={<HomePage />}></Route>
                  {user.currentUser && user.currentUser.role === 'SHIPPER' ? (
                    <>
                      <Route path='logout' element={<Logout />}></Route>˝
                      <Route path='loads' element={<MenuShipper />}></Route>
                      <Route
                        path='assigned-loads'
                        element={<MenuShipper assigned={true} />}
                      ></Route>
                      <Route path='loads-history' element={<History />}></Route>
                      <Route
                        path='generated-pdf-report'
                        element={
                          <ErrorBoundary FallbackComponent={ErrorFallback}>
                            <GenPdf />
                          </ErrorBoundary>
                        }
                      ></Route>
                    </>
                  ) : null}
                </Route>
              </Routes>
            </div>
          </div>
        </BrowserRouter>
        {/* <div style={{ width: '100%', height: '400px' }}>
          <SimpleMap/>
        </div> */}
      </div>
    </ThemeProvider>
  );
}

export default App;
