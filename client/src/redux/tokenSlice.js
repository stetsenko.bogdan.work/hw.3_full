import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  currentToken: null,
  loading: false,
  error: false,
};

export const tokenSlice = createSlice({
  name: 'token',
  initialState,
  reducers: {
    tokenSuccess: (state, action) => {
      state.loading = true;
      state.currentToken = action.payload;
    },
    tokenFailure: (state) => {
      state.loading = false;
      state.error = true;
    },
    logoutToken:(state)=>{
      state.loading = false;
      state.currentToken = '';
      state.error = false;

    }
  },
});

export const { tokenSuccess, tokenFailure,logoutToken } = tokenSlice.actions;

export default tokenSlice.reducer;
