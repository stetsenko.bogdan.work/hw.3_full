import axios from 'axios';
const appService = () => {
  const _requestBase = 'http://localhost:8080/api';
  const signUpUser = async (body) => {
    const result = await axios.post(`${_requestBase}/auth/register`, body);
    return result;
  };
  const signInUser = async (body) => {
    const result = await axios.post(`${_requestBase}/auth/login`, body);
    return result;
  };
  const userInfo = async (token) => {
    const result = await axios.get(`${_requestBase}/users/me`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const getUserLoads = async (token) => {
    const result = await axios.get(`${_requestBase}/loads`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const createLoad = async (obj, token) => {
    const result = await axios.post(
      `${_requestBase}/loads`,
      obj,
      {
        headers: { Authorization: 'Bearer ' + token },
      }
    );
    return result;
  };
  const changeCompletedById = async (id, token) => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const result = await axios.patch(`${_requestBase}/notes/${id}`);
    return result;
  };
  const postLoad = async (id, token) => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const result = await axios.post(`${_requestBase}/loads/${id}/post`);
    return result;
  };
  const updateLoad = async (id, obj, token) => {
    const result = await axios.put(
      `${_requestBase}/loads/${id}`,
      obj,
      {
        headers: { Authorization: 'Bearer ' + token },
      }
    );
    return result;
  };
  const getLoadById = async (id,token) => {
    const result = await axios.get(
      `${_requestBase}/loads/${id}`,
      {
        headers: { Authorization: 'Bearer ' + token },
      }
    );
    return result;
  };
  const deleteUser = async (token) => {
    const result = await axios.delete(`${_requestBase}/users/me`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const deleteLoad = async (id, token) => {
    const result = await axios.delete(`${_requestBase}/loads/${id}`, {
      headers: { Authorization: 'Bearer ' + token },
    });
    return result;
  };
  const updatePassword = async (body, token) => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const result = await axios.post(`${_requestBase}/auth/forgot_password`, body);
    return result;
  };
  return {
    userInfo,
    signInUser,
    signUpUser,
    getUserLoads,
    createLoad,
    changeCompletedById,
    postLoad,
    updateLoad,
    deleteUser,
    updatePassword,
    getLoadById,
    deleteLoad,
  };
};

export default appService;
