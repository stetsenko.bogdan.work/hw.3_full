import React, { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import { Link as RouterLink } from 'react-router-dom';
import appService from '../service/appService.js';
function Copyright(props) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}
    >
      {'Copyright © '}
      {new Date().toLocaleDateString()}
      {'.'}
    </Typography>
  );
}

export default function SignUp() {
  const [role, setRole] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [error, setError] = useState(null);
  const [errorPass, setErrorPass] = useState(null);
  const { signUpUser } = appService();
  const checkEmail = (email) => {
    if (email === '') {
      setEmail(email);
      setError(null);
      return;
    }
    let re = /\S+@\S+\.\S+/;
    const res = re.test(email);
    setEmail(email);
    !res ? setError(true) : setError(null);
  };
  const checkPass = (password) => {
    if (password === '') {
      setPassword(password);
      setErrorPass(null);
      return;
    }
    if (
      password.length < 5 ||
      !password.trim().length ||
      password.split(' ').length !== password.split().length
    ) {
      setErrorPass(true);
      setPassword(password);
      return;
    } else {
      setPassword(password);
      setErrorPass(null);
    }
  };
  const [open, setOpen] = useState(false);
  const handleSubmit = async (event) => {
    event.preventDefault();
    if (error || errorPass || email === '' || password === '' || role === '') {
      return;
    }
    const res = await signUpUser({
      email: email,
      password: password,
      role: role,
    });
    if (res.status === 200) {
      window.location.href = 'http://localhost:3000/login';
    }
  };

  return (
    <>
      <Collapse
        in={open}
        style={{
          position: 'absolute',
          left: '50%',
          transform: 'translateX(-50%)',
          top: '10px',
        }}
      >
        <Alert
          action={
            <IconButton
              aria-label='close'
              color='inherit'
              size='small'
              onClick={() => {
                setOpen(false);
              }}
            ></IconButton>
          }
          sx={{ mb: 2 }}
          variant='outlined'
          severity='info'
        >
          New password sent to your email address!
        </Alert>
      </Collapse>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: '#d81b60' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component='h1' variant='h5'>
            Sign up
          </Typography>
          <Box
            component='form'
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel id='demo-simple-select-label'>Role</InputLabel>
                  <Select
                    labelId='demo-simple-select-label'
                    id='demo-simple-select'
                    value={role}
                    label='Role'
                    autoFocus
                    onChange={(e) => setRole(e.target.value)}
                  >
                    <MenuItem value={'DRIVER'}>DRIVER</MenuItem>
                    <MenuItem value={'SHIPPER'}>SHIPPER</MenuItem>
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  error={error}
                  id='email'
                  label='Email Address'
                  name='email'
                  autoComplete='email'
                  value={email}
                  onChange={(e) => checkEmail(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  error={errorPass}
                  name='password'
                  label='Password'
                  type='password'
                  id='password'
                  autoComplete='new-password'
                  value={password}
                  onChange={(e) => checkPass(e.target.value)}
                  helperText={
                    errorPass
                      ? 'Minimum length 5. Should contain numbers or symbols'
                      : null
                  }
                />
              </Grid>
            </Grid>
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            <Grid
              container
              fontSize='14px'
              justifyContent='space-between'
              gap='50px'
            >
              <Grid item>
                <Link href='../login'>
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </>
  );
}
