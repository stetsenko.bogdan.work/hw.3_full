import React from 'react';
import { Link } from 'react-router-dom';
import './Sidebar.scss';
const SidebarShipper = (props) => {
  return (
    <div className={props.open ? 'sidebar' : 'sidebar hide'}>
      <ul className='sidebar-list'>
        <Link
          to='loads'
          style={{ textDecoration: 'none', color: 'inherit', width: '100%' }}
        >
          <li className='sidebar-item'>Loads</li>
        </Link>
        <Link
          to='assigned-loads'
          style={{ textDecoration: 'none', color: 'inherit', width: '100%' }}
        >
          <li className='sidebar-item'>Assigned Loads</li>
        </Link>
        <Link
          to='loads-history'
          style={{ textDecoration: 'none', color: 'inherit', width: '100%' }}
        >
        <li className='sidebar-item'>History</li></Link>
      </ul>
    </div>
  );
};

export default SidebarShipper;
