import React, { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import { useSelector, useDispatch } from 'react-redux';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import { setImage, logout } from '../redux/userSlice.js';
import { logoutToken } from '../redux/tokenSlice.js';

import appService from '../service/appService.js';
import './Navbar.scss';
import ForgotPass from './ForgotPass.jsx';
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  height: 300,
  bgcolor: '#fff',
  border: '2px solid #000',
  boxShadow: 24,
  borderRadius: '20px',
  p: 4,
};

export default function Navbar(props) {
  const user = useSelector((state) => state.user);
  const token = useSelector((state) => state.token);
  const [imageName, setImageName] = useState('');
  const [anchorEl, setAnchorEl] = useState(null);
  const [open, setOpen] = useState(false);
  const [openReset, setOpenReset] = useState(false);
  const handleOpenReset = () => setOpenReset(true);
  const handleCloseReset = () => setOpenReset(false);

  const handleOpen = () => setOpen(true);
  const dispatch = useDispatch();
  const handleCloseModal = () => setOpen(false);
  const { deleteUser } = appService();
  const dispatchImage = () => {
    console.log(imageName);
    dispatch(setImage(imageName));
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const transformDate = (date) => {
    return new Date(date).toLocaleDateString();
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const deleteAccount = async () => {
    const result = await deleteUser(token.currentToken);
    if (result.status === 200) {
      window.location.assign('http://localhost:3000/logout');
    }
  };
  return (
    <Box
      sx={{ flexGrow: 1 }}
      style={{
        zIndex: 3,
        position: 'relative',
        borderBottom: '2px solid #fff',
      }}
    >
      <ForgotPass handleOpenReset={handleOpenReset} handleCloseReset={handleCloseReset} openReset={openReset} />
      <AppBar position='static'>
        <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
          <IconButton
            size='large'
            edge='start'
            color='inherit'
            aria-label='menu'
            sx={{ mr: 2 }}
            onClick={props.toggleOpen}
          >
            <MenuIcon />
          </IconButton>
          <Link
            to={user.currentUser ? 'logout' : 'register'}
            style={{ textDecoration: 'none', color: 'inherit' }}
          >
            <Button variant='outlined'>
              {!user.currentUser ? 'Sign Up' : 'Logout'}
            </Button>
          </Link>
          {user.currentUser && (
            <div>
              <IconButton
                size='large'
                aria-label='account of current user'
                aria-controls='menu-appbar'
                aria-haspopup='true'
                onClick={handleMenu}
                color='inherit'
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id='menu-appbar'
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem onClick={handleOpen}>Profile</MenuItem>
                <MenuItem onClick={deleteAccount}>Delete Account</MenuItem>
                <MenuItem onClick={handleOpenReset}>Reset Password</MenuItem>
              </Menu>
            </div>
          )}
          <Modal
            open={open}
            onClose={handleCloseModal}
            aria-labelledby='modal-modal-title'
            aria-describedby='modal-modal-description'
          >
            <Box sx={style}>
              <Stack
                direction='row'
                alignItems='center'
                justifyContent='space-between'
                spacing={2}
                padding='0 0 30px 0'
              >
                <TextField
                  sx={{ input: { color: '#000' } }}
                  label='Img url'
                  variant='filled'
                  color='success'
                  focused
                  onChange={(e) => setImageName(e.target.value)}
                />
                <Button
                  variant='contained'
                  component='label'
                  color='success'
                  style={{ fontSize: '12px', padding: '12px 6px' }}
                  onClick={dispatchImage}
                >
                  {user.userImage ? 'Change photo' : 'Upload photo'}
                </Button>
                <img className='profile-image' src={user.userImage} alt='' />
              </Stack>
              {user.currentUser ? (
                <>
                  <Typography
                    id='modal-modal-title'
                    variant='h6'
                    component='h2'
                    sx={{
                      fontStyle: 'italic',
                      color: '#6b6b6b',
                    }}
                  >
                    {user.currentUser.email}
                  </Typography>
                  <Typography
                    id='modal-modal-description'
                    sx={{
                      mt: 2,
                      fontWeight: '700',
                      fontSize: '20px',
                      letterSpacing: '5px',
                      backgroundColor: '#c9c9c9',
                      p: '5px 20px',
                    }}
                  >
                    {user.currentUser.role}
                  </Typography>
                  <Typography id='modal-modal-date' sx={{ mt: 2 }}>
                    {'Created Date:' +
                      transformDate(user.currentUser.created_date)}
                  </Typography>
                </>
              ) : null}
            </Box>
          </Modal>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
