import React from 'react';
import './HomePage.scss'
const HomePage = () => {
  const style = {
     margin:'0 auto',
    width: '70%',
    display: 'flex',
    justifyContent: 'center',
    height: '400px',
    alignItems:'flex-start',
    flexDirection:'column'
  };
  return (
    <div style={style}>
      <div className='truck'>
        <span className='cab'></span>
        <span className='smoke'></span>
      </div>
      <hr/>
    </div>
  );
};

export default HomePage;
