import React, { useEffect, useState } from 'react';
import Pdf from 'react-to-pdf';
import appService from '../service/appService';
import { useSelector } from 'react-redux';
import { Button } from '@mui/material';
import './GenPdf.scss';
const GenPdf = () => {
  const ref = React.createRef();
  const [item, setItem] = useState({ dimensions: {}, logs: [] });
  const token = useSelector((state) => state.token);
  const { getLoadById } = appService();

  useEffect(() => {
    const getLoad = async () => {
      const id = new URLSearchParams(window.location.search).get('id');
      const newLoad = await getLoadById(id, token.currentToken);
      setItem(newLoad.data);
    };
    getLoad();
  }, []);
  let logs;
  logs = item.logs.map((item, index) => {
    return <li key={index}>{item.message}</li>;
  });
  console.log(item);
  return (
    <div className='gen-wrapper'>
      <Pdf targetRef={ref} filename='code-example.pdf'>
        {({ toPdf }) => (
          <Button variant='contained' color='success' onClick={toPdf}>
            Generate Pdf
          </Button>
        )}
      </Pdf>
      <div className='gen' ref={ref}>
        <div className='gen-item'>Name of the object: {item.name}</div>
        <div className='gen-item'>Payload of the object: {item.payload}</div>
        <div className='gen-item'>Width: {item.dimensions.width}</div>
        <div className='gen-item'>Height: {item.dimensions.height}</div>
        <div className='gen-item'>Length: {item.dimensions.length}</div>
        <div className='gen-item'>Pickup Address: {item.pickup_address}</div>
        <div className='gen-item'>
          Delivery Address: {item.delivery_address}
        </div>
        <div className='gen-item'>Created By: {item.created_by}</div>
        <div className='gen-item'>Created Date: {item.created_date}</div>
        <div className='gen-item'>
          Logs:
          <ul style={{ paddingLeft: '60px' }}>{logs}</ul>
        </div>
      </div>
    </div>
  );
};

export default GenPdf;
