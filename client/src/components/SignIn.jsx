import React, { useEffect, useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Link as RouterLink } from 'react-router-dom';
import appService from '../service/appService.js';
import { useDispatch, useSelector } from 'react-redux';
import { loginStart, loginSuccess, loginFailure } from '../redux/userSlice';
import { tokenSuccess } from '../redux/tokenSlice.js';
import ForgotPass from './ForgotPass.jsx';

function Copyright(props) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}
    >
      {'Copyright © '}
      {new Date().toLocaleDateString()}
    </Typography>
  );
}

export default function SignIn() {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [error, setError] = useState(null);
  const [errorPass, setErrorPass] = useState(null);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const checkEmail = (email) => {
    if (email === '') {
      setEmail(email);
      setError(null);
      return;
    }
    let re = /\S+@\S+\.\S+/;
    const res = re.test(email);
    setEmail(email);
    !res ? setError(true) : setError(null);
  };
  const { signInUser, userInfo } = appService();

  const checkPass = (password) => {
    if (password === '') {
      setPassword(password);
      setErrorPass(null);
      return;
    }
    if (
      password.length < 5 ||
      !password.trim().length ||
      password.split(' ').length !== password.split().length
    ) {
      setErrorPass(true);
      setPassword(password);
      return;
    } else {
      setPassword(password);
      setErrorPass(null);
    }
  };
  const handleSubmit = async (event) => {
    dispatch(loginStart());
    event.preventDefault();
    if (error || errorPass || email === '' || password === '') {
      return;
    }
    try {
      const result = await signInUser({
        email: email,
        password: password,
      });

      if (result.status === 200) {
        dispatch(tokenSuccess(result.data.jwt_token));
        const info = await userInfo(result.data.jwt_token);
        console.log(info);
        const success = new Promise((res, rej) => {
          dispatch(loginSuccess(info.data.user));
          res();
        });
        success.then(() => {
          window.location.assign('http://localhost:3000/home');
        });
      }
    } catch (err) {
      window.location.assign(`http://localhost:3000/error?error=${err}`);
    }
  };
  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: '#00e676' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign in
        </Typography>
        <Box component='form' onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin='normal'
            required
            fullWidth
            id='email'
            label='Email Address'
            name='email'
            autoComplete='email'
            autoFocus
            error={error}
            value={email}
            onChange={(e) => checkEmail(e.target.value)}
          />
          <TextField
            margin='normal'
            required
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            error={errorPass}
            autoComplete='current-password'
            value={password}
            onChange={(e) => checkPass(e.target.value)}
            helperText={
              errorPass
                ? 'Minimum length 5. Should contain numbers or symbols'
                : null
            }
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid
            container
            fontSize='14px'
            justifyContent='space-between'
            gap='50px'
          >
            <Grid item>
              <Link href='../register'>
                Don't have an account? Sign Up
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Copyright sx={{ mt: 8, mb: 4 }} />
    </Container>
  );
}
