import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import { useSelector } from 'react-redux';
import appService from '../service/appService.js';
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  height: 200,
  bgcolor: '#fff',
  boxShadow: 24,
  borderRadius: '20px',
  outline: 'none',
  p: 4,
  color: '#000',
};

const ForgotPass = ({ openReset, handleCloseReset }) => {
  const token = useSelector((state) => state.token);
  const [email, setEmail] = useState('');
  const { updatePassword } = appService();
  const handleReset = async () => {
    const result = await updatePassword({ email }, token.currentToken);
    if(result.status===200){
      window.location.assign('http://localhost:3000/logout');
    }
  };
  return (
    <div>
      <Modal
        open={openReset}
        onClose={handleCloseReset}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Stack sx={style} alignItems='center'>
          <Typography id='modal-modal-title' variant='h6' component='h2'>
            Password reset email
          </Typography>
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Email'
            id='filled-basic'
            variant='filled'
            color='success'
            focused
            name='Email'
            onChange={(e) => setEmail(e.target.value)}
          />
          <Button
            type='submit'
            fullWidth
            color='success'
            variant='contained'
            onClick={handleReset}
            sx={{ mt: 3, mb: 2, width: '200px' }}
          >
            Reset
          </Button>
        </Stack>
      </Modal>
    </div>
  );
};

export default ForgotPass;
