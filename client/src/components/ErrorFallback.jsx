import React from 'react'
const style={
  color:'#fff',
  width:'100%',
  fontSize:'50px',
  textTransform:'uppercase',
  textAlign:'center',
  letterSpacing:'5px'
}
function ErrorFallback() {
   return (
     <div role="alert" style={style}>
       <p>Something went wrong! :(</p>
     </div>
   )
 }

export default ErrorFallback