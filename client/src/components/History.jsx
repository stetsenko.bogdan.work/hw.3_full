import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import appService from '../service/appService.js';
import './History.scss';
const History = () => {
  const { getUserLoads } = appService();
  const [logs, setLogs] = useState([]);

  const token = useSelector((state) => state.token);
  useEffect(() => {
    const getLoads = async () => {
      const res = await getUserLoads(token.currentToken);
      const logs = res.data.loads
        .reduce((acc, item, index) => {
          acc[index] = item.logs.map((element) => {
            element.id = item._id;
            return element;
          });
          return acc;
        }, [])
        .flat();
      setLogs(logs);
    };
    getLoads();
  }, []);
  const items = logs.map((item) => {
    return (
      <div
        className='history-wrapper'
        key={item.id + Math.random() * Math.pow(10000, 4)}
      >
        <div className='history-id'>{item.id}</div>
        <div className='history-message'>{item.message}</div>
        <div className='history-time'>
          {new Date(item.time).toLocaleDateString('en-US', {
            hour: '2-digit',
            minute: '2-digit',
          })}
        </div>
      </div>
    );
  });
  return (
    <div className='history'>
      <div className='stats'>
        <span>Load Id</span>
        <span>Status</span>
        <span>Creation Time</span>
      </div>
      <div className='logs'>{items}</div>
    </div>
  );
};

export default History;
