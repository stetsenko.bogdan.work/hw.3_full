import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { logout } from '../redux/userSlice';
import { logoutToken } from '../redux/tokenSlice';
import CircularProgress from '@mui/material/CircularProgress';
import './Logout.scss';
const Logout = () => {
  const dispatch = useDispatch();
  const LogOut = () => {
    dispatch(logout());
    dispatch(logoutToken());
  };
  useEffect(() => {
    setTimeout(() => {
      LogOut();
      window.location.assign('http://localhost:3000/login');
    }, 1000);
  }, []);
  return (
    <div className='logout-wrapper'>
      <CircularProgress color='success' />
    </div>
  );
};

export default Logout;
