import React, { useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import SendIcon from '@mui/icons-material/Send';
import Modal from '@mui/material/Modal';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  height: 600,
  bgcolor: '#fff',
  boxShadow: 24,
  borderRadius: '20px',
  p: 4,
};
const CreateLoad = (props) => {
  const [element, setElement] = useState('DEFAULT');
  const handleChange = (e) => {
    setElement(e.target.value);
  };
  return (
    <div className='shipper-modal'>
      {!props.update ? (
        <Button
          variant='contained'
          color='success'
          onClick={props.handleOpen}
          endIcon={<SendIcon />}
        >
          Create Load
        </Button>
      ) : null}
      <Modal
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Stack
          alignItems='center'
          component='form'
          onSubmit={props.handleSubmit}
          sx={style}
        >
          <Typography id='modal-modal-title' variant='h6' component='h2'>
            {props.update ? 'Update Load' : 'Create Load'}
          </Typography>
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Name'
            id='filled-basic'
            variant='filled'
            color='success'
            focused
            name='name'
          />
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Payload'
            id='filled-basic'
            variant='filled'
            color='success'
            focused
            name='payload'
          />
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Pickup Address'
            id='filled-basic'
            variant='filled'
            color='success'
            name='pickup'
            focused
          />
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Delivery Address'
            id='filled-basic'
            variant='filled'
            color='success'
            name='delivery'
            focused
          />
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Width'
            id='filled-basic'
            variant='filled'
            color='success'
            name='width'
            focused
          />
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Height'
            id='filled-basic'
            variant='filled'
            color='success'
            name='height'
            focused
          />
          <TextField
            sx={{ input: { color: '#000' } }}
            label='Length'
            id='filled-basic'
            variant='filled'
            color='success'
            name='length'
            focused
          />
          <Button
            type='submit'
            fullWidth
            color='success'
            variant='contained'
            sx={{ mt: 3, mb: 2, width: '200px' }}
          >
            {props.update ? 'Update' : 'Create'}
          </Button>
        </Stack>
      </Modal>
      {!props.update ? (
        <div className='filter'>
          <span>Filter</span>
          <select
            name='select'
            value={element}
            onChange={(e) => {
              handleChange(e);
              props.setFilter(e.target.value);
            }}
          >
            <option value='DEFAULT'>DEFAULT</option>
            <option value='NEW'>NEW</option>
            <option value='ASSIGNED'>ASSIGNED</option>
            <option value='SHIPPED'>SHIPPED</option>
          </select>
        </div>
      ) : null}
    </div>
  );
};

export default CreateLoad;
