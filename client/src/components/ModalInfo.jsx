import React from 'react';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Stack from '@mui/material/Stack';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  height: 300,
  bgcolor: '#fff',
  boxShadow: 24,
  borderRadius: '20px',
  outline: 'none',
  p: 4,
};
const topStyle = {
  textAlign: 'center',
  textTransform: 'uppercase',
  fontWeight: '700',
};

const itemStyle = {
  textTransform: 'uppercase',
  fontWeight: '700',
  p: '5px 20px',
  backgroundColor: '#6b6b6b',
  color: '#fff',
};
const itemStylePayload = {
   textTransform: 'uppercase',
   fontWeight: '700',
   p: '5px 20px',
   backgroundColor: '#ff7300',
   color: '#fff',
 };
const ModalInfo = (props) => {
  return (
    <div className='load-info'>
      <Modal
        open={props.openInfo}
        onClose={props.handleCloseInfo}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Stack sx={style} justifyContent='space-between'>
          <Typography
            id='modal-modal-title'
            variant='h6'
            component='h2'
            sx={topStyle}
          >
            Load info
          </Typography>
          <Typography id='modal-modal-payload' sx={itemStylePayload}>
            {'Peyload: ' + props.item.payload}
          </Typography>
          <Typography id='modal-modal-weight' sx={itemStyle}>
            {'Width: ' + props.item.width}
          </Typography>
          <Typography id='modal-modal-height' sx={itemStyle}>
            {'Height: ' + props.item.height}
          </Typography>
          <Typography id='modal-modal-length' sx={itemStyle}>
            {'Length: ' + props.item.length}
          </Typography>
        </Stack>
      </Modal>
    </div>
  );
};

export default ModalInfo;
