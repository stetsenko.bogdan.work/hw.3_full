import React, { createRef } from 'react';
import CandlestickChartIcon from '@mui/icons-material/CandlestickChart';
import EditIcon from '@mui/icons-material/Edit';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import { Link } from 'react-router-dom';
import DeleteIcon from '@mui/icons-material/Delete';
const Load = ({
  item,
  handleOpenInfo,
  handleOpenUpdate,
  handlePost,
  deleteUserLoad,
}) => {
  return (
    <>
      <tr>
        <td className='load-list__name'>{item.name}</td>
        <td className='load-list__date'>
          {new Date(item.created_date).toLocaleDateString('en-US', {
            hour: '2-digit',
            minute: '2-digit',
          })}
        </td>
        <td className='load-list__start'>{item.pickup_address}</td>
        <td className='load-list__end'>{item.delivery_address}</td>
        <td className='load-list__post'> {item.status}</td>
        <td className='load-list__post' onClick={() => handleOpenInfo(item)}>
          <CandlestickChartIcon />
        </td>
        <td
          className='load-list__edit'
          onClick={() => handleOpenUpdate(item._id)}
        >
          <EditIcon />
        </td>
        <td className='load-list__post' onClick={() => handlePost(item._id)}>
          <LocalShippingIcon />
        </td>
        <td className='load-list__post'>
          <Link
            to={`../generated-pdf-report/?id=${item._id}`}
            style={{ textDecoration: 'none', color: 'inherit' }}
          >
            <PictureAsPdfIcon />
          </Link>
        </td>
        <td
          className='load-list__delete'
          onClick={() => deleteUserLoad(item._id)}
        >
          <DeleteIcon className='load-list__delete' />
        </td>
      </tr>
    </>
  );
};

export default Load;
