import React, { useEffect, useState } from 'react';
import './MenuShipper.scss';
import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';
import appService from '../service/appService.js';
import usePagination from './Pagination';
import { useSelector } from 'react-redux';
import ModalInfo from './ModalInfo';
import ModalLoad from './ModalLoad';
import Load from './Load';

const MenuShipper = (props) => {
  const [open, setOpen] = useState(false);
  const [filt, setFilt] = useState('DEFAULT');
  const [loads, setLoads] = useState([]);
  const token = useSelector((state) => state.token);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [item, setItem] = useState({
    payload: 0,
    width: 0,
    height: 0,
    length: 0,
  });
  const [itemId, setItemId] = useState(null);

  const [openInfo, setOpenInfo] = useState(false);
  const [openUpdate, setOpenUpdate] = useState(false);

  const handleOpenUpdate = (id) => {
    setItemId(id);
    setOpenUpdate(true);
  };
  const handleOpenPost = (id) => {
    setItemId(id);
  };
  const handleCloseUpdate = () => setOpenUpdate(false);
  const handleOpenInfo = (item) => {
    setItem({
      payload: item.payload,
      width: item.dimensions.width,
      length: item.dimensions.length,
      height: item.dimensions.height,
    });
    setOpenInfo(true);
  };
  const handleCloseInfo = () => setOpenInfo(false);
  const {
    getUserLoads,
    createLoad,
    updateLoad,
    getLoadById,
    postLoad,
    deleteLoad,
  } = appService();
  useEffect(() => {
    const getLoads = async () => {
      const res = await getUserLoads(token.currentToken);
      setLoads(res.data.loads);
    };
    getLoads();
  }, []);
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const obj = {
      name: data.get('name'),
      payload: +data.get('payload'),
      pickup_address: data.get('pickup'),
      delivery_address: data.get('delivery'),
      dimensions: {
        width: +data.get('width'),
        length: +data.get('length'),
        height: +data.get('height'),
      },
    };
    const res = await createLoad(obj, token.currentToken);
    if (res.status === 200) {
      const arr = await getUserLoads(token.currentToken);
      setLoads(arr.data.loads);
      handleClose();
    }
  };
  const handleUpdate = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const obj = {
      name: data.get('name'),
      payload: +data.get('payload'),
      pickup_address: data.get('pickup'),
      delivery_address: data.get('delivery'),
      dimensions: {
        width: +data.get('width'),
        length: +data.get('length'),
        height: +data.get('height'),
      },
    };
    const res = await updateLoad(itemId, obj, token.currentToken);
    if (res.status === 200) {
      const newLoad = await getLoadById(itemId, token.currentToken);
      setLoads(
        loads.map((item) => {
          if (item._id === itemId) {
            item = newLoad.data;
            return item;
          } else {
            return item;
          }
        })
      );
      handleCloseUpdate();
    }
  };
  const handlePost = async (id) => {
    setItemId(id);
    const result = await postLoad(id, token.currentToken);
    const arr = await getUserLoads(token.currentToken);
    setLoads(arr.data.loads);
  };
  const deleteUserLoad = async (id) => {
    const result = await deleteLoad(id, token.currentToken);
    const arr = await getUserLoads(token.currentToken);
    setLoads(arr.data.loads);
  };
  let [page, setPage] = useState(1);
  const PER_PAGE = 7;

  const count = Math.ceil(loads.length / PER_PAGE);
  const _DATA = usePagination(loads, PER_PAGE);
  const handleChange = (e, p) => {
    setPage(p);
    _DATA.jump(p);
  };
  const setFilter = (value) => {
    setFilt(value);
  };
  let items;
  items = _DATA.currentData().map((item, index) => {
    if (props.assigned) {
      if (item.status !== 'NEW') {
        return (
          <Load
            key={item._id}
            item={item}
            handleOpenInfo={handleOpenInfo}
            handleOpenUpdate={handleOpenUpdate}
            handlePost={handlePost}
            deleteUserLoad={deleteUserLoad}
          />
        );
      }
    } else {
      return (
        <Load
          key={item._id}
          item={item}
          handleOpenInfo={handleOpenInfo}
          handleOpenUpdate={handleOpenUpdate}
          handlePost={handlePost}
          deleteUserLoad={deleteUserLoad}
        />
      );
    }
  });
  if (filt !== 'DEFAULT') {
    items = items.filter((elem) => {
      console.log(elem.props.item.status === filt);
      console.log(filt);
      if (elem.props.item.status === filt) {
        return elem;
      }
    });
  }
  return (
    <div className='menu-shipper-wrapper'>
      <div className='menu-shipper-content'>
        {props.assigned ? null : (
          <ModalLoad
            setFilter={setFilter}
            update={false}
            open={open}
            handleClose={handleClose}
            handleOpen={handleOpen}
            handleSubmit={handleSubmit}
          />
        )}
        <ModalLoad
          update={true}
          open={openUpdate}
          handleClose={handleCloseUpdate}
          handleOpen={handleOpenUpdate}
          handleSubmit={handleUpdate}
        />
        <ModalInfo
          openInfo={openInfo}
          handleCloseInfo={handleCloseInfo}
          item={item}
        />
        <div className='tbl-header'>
          <table cellPadding='0' cellSpacing='0' border='0'>
            <thead>
              <tr>
                <th className='table-names__item'>Load Name</th>
                <th className='table-names__item'>Created Date</th>
                <th className='table-names__item'>Pick-Up Address</th>
                <th className='table-names__item'>Delivery Address</th>
                <th className='table-names__item'>Status</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </thead>
          </table>
        </div>
        <div className='tbl-content'>
          <table
            cellPadding='0'
            cellSpacing='0'
            border='0'
            style={{ overflow: 'scroll' }}
          >
            <tbody>{items}</tbody>
          </table>
        </div>
        <Stack alignItems='center'>
          <Pagination
            count={count}
            size='large'
            page={page}
            onChange={handleChange}
          />
        </Stack>
      </div>
    </div>
  );
};

export default MenuShipper;
