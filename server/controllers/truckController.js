import { createError } from '../createError.js';
import Truck, { trackJoiSchema } from '../models/Truck.js';
import User from '../models/User.js';
import Load from '../models/Load.js';
export const addUserTruck = async (req, res, next) => {
  const findUser = await User.findById(req.user.userId);
  if (findUser.role === 'DRIVER') {
    const newTruck = new Truck({ ...req.body });
    newTruck.created_by = req.user.userId;
    newTruck.save().then(() => {
      res.json({
        message: 'Truck created successfully',
      });
    });
  } else {
    next(createError(400));
  }
};

export const assignTruck = async (req, res, next) => {
  const truck = Truck.findOne({
    _id: req.params.id,
    created_by: req.user.userId,
  });
  if (!truck) {
    next(createError(400));
  }
  if (truck.status === 'OL') {
    next(createError(400));
  }
  const findTrucks = await Truck.find({ assigned_to: req.user.userId });
  if (findTrucks.length === 1) {
    next(createError(400));
  } else {
    await truck.updateOne({ $set: { assigned_to: req.user.userId } });
    res.json({
      message: 'Truck assigned successfully',
    });
  }
};

export const getUserTrucks = async (req, res, next) => {
  const findUser = await User.findById(req.user.userId);
  if (findUser.role === 'DRIVER') {
    const trucks = await Truck.find({ created_by: req.user.userId });
    if (!trucks) {
      next(createError(400));
    }
    res.json({
      trucks,
    });
  } else {
    next(createError(400));
  }
};

export const getUserTruckById = async (req, res, next) => {
  const truck = await Truck.findById(req.params.id);
  if (!truck) {
    next(createError(400));
  }
  res.json(truck);
};

export const updateTruck = async (req, res, next) => {
  const truck = Truck.findById(req.params.id);
  if (!truck || Object.keys(req.body).length < 1) {
    next(createError(400));
  } else {
    await truck.updateOne(req.body);
    res.json({
      message: 'Truck details changed successfully',
    });
  }
};

export const deleteUserTruckById = async (req, res, next) => {
  const truck = await Truck.findOne(req.params.id);
  const load = await Load.findById(truck.created_by);
  if (!load) {
    next(createError(400));
  }
  truck.deleteOne();
  if (load) {
    await load.updateOne({
      $set: {
        assigned_to: null,
        status: 'NEW',
      },
    });
  }
  res.json({
    message: 'Truck deleted successfully',
  });
};
