import {
  saveCredential,
  saveNewUser,
  saveRegCredential,
} from '../services/authService.js';
import nodemailer from 'nodemailer';
import { userJoiSchema } from '../models/User.js';
import { createError } from '../createError.js';
import User from '../models/User.js';
import Credential from '../models/Credential.js';
import RegistrationCredential from '../models/RegistrationCredential.js';
import generator from 'generate-password';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { registrationCredentialJoiSchema } from '../models/RegistrationCredential.js';

export const registerNewUser = async (req, res, next) => {
  userJoiSchema
    .validateAsync(req.body)
    .then(
      registrationCredentialJoiSchema.validateAsync({
        password: req.body.password,
      })
    )
    .catch(() => {
      next(createError(400));
    });
  saveNewUser(req.body)
    .then((item) => {
      saveRegCredential(req.body, item._id);
      saveCredential(req.body, item._id);
      res.json({
        message: 'Profile created successfully',
      });
    })
    .catch(400);
};

export const loginUser = async (req, res, next) => {
  const findUser = await User.findOne({ email: req.body.email });
  const findCredential = await Credential.findOne({ userId: findUser._id });
  if (!findUser) {
    next(createError(400));
  }
  const checkPassword = await bcrypt.compare(
    req.body.password,
    findCredential.password
  );
  if (!checkPassword) {
    next(createError(400));
  }
  const token = jwt.sign({ id: findUser._id }, process.env.KEY);
  res.json({ jwt_token: token });
};

export const forgotPassword = async (req, res, next) => {
  if (!req.body.email) {
    next(createError(400));
  }
  const credential = Credential.findOne({ userId: req.user.userId });
  const regCredential = RegistrationCredential.findOne({
    userId: req.user.userId,
  });
  if (!credential || !regCredential) {
    next(createError(400));
  }
  async function main() {
    let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 587,
      secure: false,
      auth: {
        user: 'stetsenko.bogdan.work@gmail.com',
        pass: 'mngbumftkempbnzt',
      },
    });
    let password = generator.generate({
      length: 25,
      numbers: true,
    });
    await credential.updateOne({ password: await bcrypt.hash(password, 10) });
    await regCredential.updateOne({ password: await bcrypt.hash(password, 10) });
    let info = await transporter.sendMail({
      from: '"NEW PASSWORD" stetsenko.bogdan.work@gmail.com',
      to: req.body.email,
      subject: '⚙️ YOUR NEW PASSWORD ⚙️',
      html: `<b>Hello! Your new password is:\n<em style="color:orange">${password}</em></p>`,
    });
  }
  main().then(() => {
    res.json({
      message: 'New password sent to your email address',
    });
  });
};
