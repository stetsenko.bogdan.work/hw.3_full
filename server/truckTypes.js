const SPRINTER = {
  name: 'SPRINTER',
  width: 300,
  length: 250,
  height: 170,
  capacity: 1700,
};
const SMALL_STRAIGHT = {
  name: 'SMALL STRAIGHT',
  width: 300,
  length: 250,
  height: 170,
  capacity: 2500,
};

const LARGE_STRAIGHT = {
  name: 'LARGE STRAIGHT',
  width: 300,
  length: 250,
  height: 170,
  capacity: 4000,
};

function getType(type) {
  switch (type) {
    case 'SPRINTER':
      return SPRINTER;
    case 'SMALL STRAIGHT':
      return SMALL_STRAIGHT;
    case 'LARGE STRAIGHT':
      return LARGE_STRAIGHT;
  }
}
export { SPRINTER, SMALL_STRAIGHT, LARGE_STRAIGHT, getType };
