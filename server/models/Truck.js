import mongoose from 'mongoose';
import Joi from 'joi';
const trackJoiSchema = Joi.object({
  type: Joi.string().required(),
  status: Joi.string(),
});
const Truck = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default:null,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    default: 'SPRINTER',
    required: true,
  },
  shipperId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
  },
});
export { trackJoiSchema };
export default mongoose.model('Truck', Truck);
