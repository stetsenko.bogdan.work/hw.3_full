import mongoose from 'mongoose';
import Joi from 'joi';
const userJoiSchema = Joi.object({
  username: Joi.string().alphanum(),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ['com', 'net', 'uk'] },
    })
    .required(),
  password: Joi.string().min(3).alphanum().required(),
  role: Joi.string()
    .valid(...['DRIVER', 'SHIPPER'])
    .required(),
});
const User = new mongoose.Schema({
  username: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
  },
});
export { userJoiSchema };

export default mongoose.model('User', User);
