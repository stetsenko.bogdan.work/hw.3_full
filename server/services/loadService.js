import Load from '../models/Load.js';
import bcrypt from 'bcryptjs';
export const saveNewLoad = async (req) => {
  const newLoad = new Load({
    ...req.body,
  });
  newLoad.created_by = req.user.userId;
  return await newLoad.save();
};
