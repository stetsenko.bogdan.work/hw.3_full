import User from '../models/User.js';
import Credential from '../models/Credential.js';
import RegistrationCredential from '../models/RegistrationCredential.js'
import bcrypt from 'bcryptjs';
export const saveNewUser = async (user) => {
  const { email, role, username } = user;
  const newUser = new User({
    email,
    role,
    username,
  });
  return await newUser.save();
};

export const saveCredential = async (user, _id) => {
  const { email, password } = user;
  const newCredential = new Credential({
    userId: _id,
    email,
    password: await bcrypt.hash(password, 10),
  });
  return await newCredential.save();
};

export const saveRegCredential = async(user,_id)=>{
  const { email, role, password } = user;
  const newCredential = new RegistrationCredential({
    userId: _id,
    email,
    role,
    password: await bcrypt.hash(password, 10),
  });
  return await newCredential.save();
}