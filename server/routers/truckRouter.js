import express from 'express';
import authMiddleware from '../middleware/authMiddleware.js';
import asyncWrapper from '../asyncWrapper.js';
import {addUserTruck, assignTruck, getUserTruckById, getUserTrucks} from '../controllers/truckController.js'
const router = express.Router();

//get users trucks
router.get('/', authMiddleware, asyncWrapper(getUserTrucks));
//Create user truck
router.post('/', authMiddleware, asyncWrapper(addUserTruck));
//Assign truck
router.post('/:id/assign', authMiddleware, asyncWrapper(assignTruck));
//Get user truck by id
router.get('/:id', authMiddleware, asyncWrapper(getUserTruckById));
export default router;
