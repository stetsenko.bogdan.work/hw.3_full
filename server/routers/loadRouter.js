import express from 'express';
import {
  createUserLoad,
  getUserActiveLoad,
  getUserLoads,
  postUserLoadById,
  getShippingInfo,
  deleteUserLoadById,
  updateLoad,
  getUserLoadById,
  IterateNextState,
} from '../controllers/loadController.js';
import authMiddleware from '../middleware/authMiddleware.js';
import asyncWrapper from '../asyncWrapper.js';
const router = express.Router();

//get users loads
router.get('/', authMiddleware, asyncWrapper(getUserLoads));
//Create user load
router.post('/', authMiddleware, asyncWrapper(createUserLoad));
//Post user load by id
router.post('/:id/post', authMiddleware, asyncWrapper(postUserLoadById));
//Get user's active load
router.get('/active', authMiddleware, asyncWrapper(getUserActiveLoad));
//Get user's active load
router.get('/:id/shipping_info', authMiddleware, asyncWrapper(getShippingInfo));
//Delete load
router.delete('/:id', authMiddleware, asyncWrapper(deleteUserLoadById));
//Update load
router.put('/:id', authMiddleware, asyncWrapper(updateLoad));
//Get user load by id
router.get('/:id', authMiddleware, asyncWrapper(getUserLoadById));
//iterate to next state
router.patch('/active/state', authMiddleware, asyncWrapper(IterateNextState));
export default router;
