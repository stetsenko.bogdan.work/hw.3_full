import express from 'express';
import authMiddleware from '../middleware/authMiddleware.js';
import asyncWrapper from '../asyncWrapper.js';
import {
  deleteUser,
  getUserInfo,
  updatePassword,
} from '../controllers/userController.js';
const router = express.Router();

//get user info
router.get('/', authMiddleware, asyncWrapper(getUserInfo));
//Update user password
router.patch('/password', authMiddleware, asyncWrapper(updatePassword));
//Delete truck
router.delete('/', authMiddleware, asyncWrapper(deleteUser));
export default router;
